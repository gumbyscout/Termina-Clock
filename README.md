![](media/banner_basalt.png "Avoid meeting with a terrible fate... Get Termina Clock for Pebble")

### Pebble Time
![Clock Tower Day](media/ct_day.png)
![Clock Tower Night](media/ct_night.png)
![Domestic Day](media/dc_day.png)
![Domestic Night](media/dc_night.png)

### Pebble
![Clock Tower Day B&W](media/ct_day_bw.png)
![Clock Tower Night B&W](media/ct_night_bw.png)
![Domestic Day B&W](media/dc_day_bw.png)
![Domestic Night B&W](media/dc_night_bw.png)

[Rebble Store Link](https://store-beta.rebble.io/app/558758d949c1d1c2f9000066)


---

### Libraries Used
[Pebble Assist](https://github.com/smallstoneapps/pebble-assist) - for macros

[GPath Builder](https://github.com/pebble-hacks/gpath-bezier) - for building paths with curves

[Pebble Tiny Math](https://github.com/mhungerford/pebble-tinymath) - for using less floats
