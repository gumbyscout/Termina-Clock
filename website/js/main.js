//the option variable objects
var clockFaces = [
        {
            name: "Clock Tower",
            id: "ct",
            images: {
                aplite: "media/ct_night_bw.png",
                basalt: "media/ct_night.png",
                chalk: "media/ct_day_chalk.png"
            },
            minVersion: "2.0",
            platforms: ["aplite", "basalt", "chalk"]
        }, {
            name: "Domestic",
            id: "dc",
            images: {
                aplite: "media/dc_night_bw.png",
                basalt: "media/dc_night.png",
                chalk: "media/dc_day_chalk.png"
            },
            minVersion: "2.0",
            platforms: ["aplite", "basalt", "chalk"]
        }, {
            name: "Clock Tower BW",
            id: "ctbw",
            images: {
                basalt: "media/ct_night_bw.png",
                chalk: "media/ct_night_chalk_bw.png"
            },
            minVersion: "2.3",
            platforms: ["basalt", "chalk"]
        }, {
            name: "Domestic BW",
            id: "dcbw",
            images: {
                basalt: "media/dc_night_bw.png",
                chalk: "media/dc_night_chalk_bw.png"
            },
            minVersion: "2.3",
            platforms: ["basalt", "chalk"]
        }
    ],
    backgrounds = [
        {
            name: "Stone Tile",
            id: "stone",
            images: {
                aplite: "resources/images/common/stone_tile_bw.png",
                basalt: "resources/images/common/stone_tile.png",
                chalk: "resources/images/common/stone_tile.png"
            },
            minVersion: "2.0",
            platforms: ["aplite", "basalt", "chalk"]
        }, {
            name: "Stone Tile BW",
            id: "stonebw",
            images: {
                basalt: "resources/images/common/stone_tile_bw.png",
                chalk: "resources/images/common/stone_tile_bw.png"
            },
            minVersion: "2.3",
            platforms: ["basalt", "chalk"]
        }, {
            name: "White",
            id: "white",
            color: "white",
            minVersion: "2.0",
            platforms: ["aplite", "basalt", "chalk"]
        }, {
            name: "Black",
            id: "black",
            color: "black",
            minVersion: "2.3",
            platforms: ["aplite", "basalt", "chalk"]
        }
    ],
    rotations = [
        {
            name: "Clockwise (default)",
            id: "cw",
            icon: "fa-repeat",
            minVersion: "2.1",
            platforms: ["aplite", "basalt", "chalk"]
        }, {
            name: "Counter-clockwise",
            id: "ccw",
            icon: "fa-undo",
            minVersion: "2.1",
            platforms: ["aplite", "basalt", "chalk"]
        }, {
            name: "Minutes Counter-clockwise",
            id: "cw-ccw",
            icon: "fa-circle-o",
            minVersion: "2.1",
            platforms: ["aplite", "basalt", "chalk"]
        }, {
            name: "Hours Counter-clockwise",
            id: "ccw-cw",
            icon: "fa-circle",
            minVersion: "2.1",
            platforms: ["aplite", "basalt", "chalk"]
        }
    ],
    vc = VersionCompare;

var parseParams = function (queryStr) {
    var paramPairs = queryStr.split('&'),
        params = [];

    _.forEach(paramPairs, function (pair) {
        var pairArr = pair.split('=');
        pairArr[1] = decodeURIComponent(pairArr[1]);
        params.push(pairArr);
    });

    return _.zipObject(params);
};

$(function () {
    var configForm = $('#config');

    var params = parseParams(location.search.substring(1));

    //set default return path
    _.defaults(params, {
        'return_to': "pebblejs://close#",
        'version': '2.0',
        'platform': 'basalt',
        'clock-face': 'ct',
        'background': 'stone',
        'rotation': 'cw',
        'meta': "True"
    });

    var imports = {
        'vc': VersionCompare
    };

    //add the config objects to the same scope
    var scope = _.extend({p: params});
    configForm.html(JST['configTemplate.html'](scope));

    //slate stuff
    $('.item-toggle').itemToggle();
    $('.item-checkbox').itemCheckbox();
    $('.item-select').itemSelect();
    $('.item-date').itemDate();
    $('.item-time').itemTime();
    $('.item-radio').itemRadio();
    $('.item-color-normal').itemColor({sunny: false});
    $('.item-color-sunny').itemColor({sunny: true});
    $('.tab-button').tab();
    $('.item-slider').itemSlider();
    $('.item-draggable-list').itemDraggableList();
    $('.item-dynamic-list').itemDynamicList();

    configForm.submit(function (e) {
        e.preventDefault();
        var configFormObj = encodeURIComponent(JSON.stringify(parseParams(configForm.serialize())));
        console.log("sending", decodeURIComponent(configFormObj));
        document.location = params.return_to + configFormObj;
    });
});
