var gulp = require('gulp'),
    sourcemaps = require('gulp-sourcemaps'),
    print = require('gulp-print'),
    replace = require('gulp-replace'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifyCSS = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    mainBowerFiles = require('main-bower-files'),
    plumber = require('gulp-plumber'),
    template = require('gulp-template-compile');

gulp.task('bower-css', function () {
    //concat those 3rd party css files
    return gulp.src(mainBowerFiles({
            includeDev: "exclusive",
            filter: "**/*.css"
        }))
        .pipe(sourcemaps.init())
            .pipe(replace("/font/", "/fonts/")) //fontawesome to same folder
            .pipe(concat('bower.css'))
            .pipe(minifyCSS())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./build'));
});

gulp.task('bower-fonts', function () {
    //this task is for moving fonts
    return gulp.src(mainBowerFiles({
            includeDev: "exclusive",
            filter: ["**/*.ttf", "**/*.woff", "**/*.woff2", "**/*.otf", "**/*.eot", "**/*.svg"]
        }))
        .pipe(gulp.dest('./fonts'));
});

gulp.task('bower-js', function () {
    return gulp.src(mainBowerFiles({
            includeDev: "exclusive",
            filter: ["**/*.js"]
        }))
        .pipe(sourcemaps.init())
            .pipe(concat('bower.js'))
            .pipe(uglify())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./build'));
});

gulp.task('bower', ['bower-css', 'bower-fonts', 'bower-js']);

gulp.task('css', function () {
    return gulp.src('./scss/*.scss')
        .pipe(plumber())
        .pipe(sourcemaps.init())
            .pipe(sass())
            .pipe(concat('main.css'))
            .pipe(autoprefixer())
            .pipe(minifyCSS())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./build'));
});

gulp.task('javascript', function () {
    return gulp.src('./js/*.js')
        .pipe(plumber())
        .pipe(sourcemaps.init())
            .pipe(concat('main.js'))
            .pipe(uglify())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./build'));
});

gulp.task('templates', function () {
    return gulp.src('./templates/*.html')
        .pipe(plumber())
        .pipe(template())
        .pipe(concat('templates.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./build'));
});

gulp.task('default', ['javascript', 'css', 'templates', 'bower']);

gulp.task('watch', function () {
    gulp.watch(['./js/*.js'], ['javascript']);
    gulp.watch(['./scss/*.scss'], ['css']);
    gulp.watch(['./templates/*.html'], ['templates']);
});
