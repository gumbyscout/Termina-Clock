#pragma  once
#include <pebble.h>
#include "lib/gpath_builder/gpath_builder.h"

#define MAX_POINTS 256
#define  PATH_VECTOR_INITIAL_CAPACITY 10

//definte building objects
typedef struct {
    char command;
    GPoint points[3];
} GPathBuilderCommandItem;

typedef struct {
    uint32_t num_commands;
    GPathBuilderCommandItem commands[];
} GPathBuilderCommandList;

GPath *gpath_builder_create_path_from_commands(const GPathBuilderCommandList *command_list);

//define vector to store paths
// typedef struct {
//     uint16_t size;
//     uint16_t capacity;
//     GPath **data;
// } GPathVector;
//
// void vector_init(GPathVector *vector);
//
// void vector_append(GPathVector *vector, GPath *value);
//
// GPath *vector_get(GPathVector *vector, uint16_t index);
//
// void vector_set(GPathVector *vector, uint16_t index, GPath *value);
//
// void vector_double_capacity_if_full(GPathVector *vector);
//
// void vector_free(GPathVector *vector);
