#include <pebble.h>
#include "shapes.h"

void draw_deco(GContext *ctx, GPath *deco, GPoint origin, float angle) {
    gpath_rotate_to(deco, angle);
    gpath_move_to(deco, origin);
    gpath_draw_filled(ctx, deco);
}
