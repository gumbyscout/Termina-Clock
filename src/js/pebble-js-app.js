var watchInfo,
    configKeys = [
        "clock-face", "background", "rotation", "meta"
    ],
    switches = {
        "meta": true
    },
    version = "2.4";

Pebble.addEventListener('ready', function (e) {
    if (Pebble.getActiveWatchInfo) {
        try {
            watchInfo = Pebble.getActiveWatchInfo();
        } catch (except) {
            watchInfo  = {
                "platform": "basalt"
            };
        }
    } else {
        //assume aplite
        watchInfo = {
            "platform": "aplite"
        };
    }
});

Pebble.addEventListener('appmessage', function (e) {
    console.log('payload', JSON.stringify(e.payload));
});

Pebble.addEventListener('showConfiguration', function (e) {
    var queryStr = "?platform=" + encodeURIComponent(watchInfo.platform) + "&version=" + encodeURIComponent(version);

    _.forEach(configKeys, function (key) {
        var config = localStorage.getItem(key);
        if (!_.isUndefined(config) && !_.isNull(config)) {
            queryStr += "&" + key + '=' + encodeURIComponent(config);
        }
    });

    Pebble.openURL('http://gumbyscout.github.io/Termina-Clock/index.html' + queryStr);
});

Pebble.addEventListener('webviewclosed', function (e) {
    var config = JSON.parse(decodeURIComponent(e.response));
    _.forEach(configKeys, function (key) {
        var configItem = config[key];
        if (_.has(switches, key)) {
            configItem = configItem ? true : false;
            config[key] = configItem;
        }
        if (!_.isUndefined(configItem)) {
            localStorage.setItem(key, configItem);
        }
    });
    console.log('To Pebble:', JSON.stringify(config));
    Pebble.sendAppMessage(config);
});
