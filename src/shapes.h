#include <pebble.h>
#include "shapes_util.h"

//images
static GBitmap *s_bluetooth_bitmap;
static GBitmap *s_hours_face;
static GBitmap *s_sun_moon;
static GBitmap *s_meta_one;
static GBitmap *s_meta_two;

#ifdef PBL_COLOR

    static const uint32_t s_ct_ids[] = {
        RESOURCE_ID_CT_00,
        RESOURCE_ID_CT_01,
        RESOURCE_ID_CT_02,
        RESOURCE_ID_CT_03,
        RESOURCE_ID_CT_04,
        RESOURCE_ID_CT_05,
        RESOURCE_ID_CT_06,
        RESOURCE_ID_CT_07,
        RESOURCE_ID_CT_08,
        RESOURCE_ID_CT_09,
        RESOURCE_ID_CT_10,
        RESOURCE_ID_CT_11
    };

    static const uint32_t s_dc_ids[] = {
        RESOURCE_ID_DC_00,
        RESOURCE_ID_DC_01,
        RESOURCE_ID_DC_02,
        RESOURCE_ID_DC_03,
        RESOURCE_ID_DC_04,
        RESOURCE_ID_DC_05,
        RESOURCE_ID_DC_06,
        RESOURCE_ID_DC_07,
        RESOURCE_ID_DC_08,
        RESOURCE_ID_DC_09,
        RESOURCE_ID_DC_10,
        RESOURCE_ID_DC_11
    };

    static const uint32_t s_ctbw_ids[] = {
        RESOURCE_ID_CTBW_00,
        RESOURCE_ID_CTBW_01,
        RESOURCE_ID_CTBW_02,
        RESOURCE_ID_CTBW_03,
        RESOURCE_ID_CTBW_04,
        RESOURCE_ID_CTBW_05,
        RESOURCE_ID_CTBW_06,
        RESOURCE_ID_CTBW_07,
        RESOURCE_ID_CTBW_08,
        RESOURCE_ID_CTBW_09,
        RESOURCE_ID_CTBW_10,
        RESOURCE_ID_CTBW_11
    };

    static const uint32_t s_dcbw_ids[] = {
        RESOURCE_ID_DCBW_00,
        RESOURCE_ID_DCBW_01,
        RESOURCE_ID_DCBW_02,
        RESOURCE_ID_DCBW_03,
        RESOURCE_ID_DCBW_04,
        RESOURCE_ID_DCBW_05,
        RESOURCE_ID_DCBW_06,
        RESOURCE_ID_DCBW_07,
        RESOURCE_ID_DCBW_08,
        RESOURCE_ID_DCBW_09,
        RESOURCE_ID_DCBW_10,
        RESOURCE_ID_DCBW_11
    };
#elif PBL_BW

    static const uint32_t s_ct_ids[] = {
        RESOURCE_ID_CTBW_00,
        RESOURCE_ID_CTBW_01,
        RESOURCE_ID_CTBW_02,
        RESOURCE_ID_CTBW_03,
        RESOURCE_ID_CTBW_04,
        RESOURCE_ID_CTBW_05,
        RESOURCE_ID_CTBW_06,
        RESOURCE_ID_CTBW_07,
        RESOURCE_ID_CTBW_08,
        RESOURCE_ID_CTBW_09,
        RESOURCE_ID_CTBW_10,
        RESOURCE_ID_CTBW_11
    };

    static const uint32_t s_dc_ids[] = {
        RESOURCE_ID_DCBW_00,
        RESOURCE_ID_DCBW_01,
        RESOURCE_ID_DCBW_02,
        RESOURCE_ID_DCBW_03,
        RESOURCE_ID_DCBW_04,
        RESOURCE_ID_DCBW_05,
        RESOURCE_ID_DCBW_06,
        RESOURCE_ID_DCBW_07,
        RESOURCE_ID_DCBW_08,
        RESOURCE_ID_DCBW_09,
        RESOURCE_ID_DCBW_10,
        RESOURCE_ID_DCBW_11
    };
#endif

static GPath *s_arrow;

#ifdef PBL_RECT
static const GPathInfo ARROW = {
    6,
    (GPoint []) {
        {65, 23},
        {71, 10},
        {72, 10},
        {78, 23},
        {72, 28},
        {71, 28}
    }
};
#elif PBL_ROUND
static const GPathInfo ARROW = {
    6,
    (GPoint []) {
        {84, 28},
        {90, 15},
        {91, 15},
        {97, 28},
        {91, 33},
        {90, 33}
    }
};
#endif

static GPath *s_diamond;

static const GPathInfo DIAMOND = {
    8,
    (GPoint []) {
        {-1, -20},
        {1, -20},
        {15, 1},
        {15, -1},
        {1, 20},
        {-1, 20},
        {-15, 1},
        {-15, -1}
    }
};


static GPath *s_minute;

static const GPathBuilderCommandList s_minute_comm = {
    5,
    {
        {
            'm',
            {{-1, 9}}
        }, {
            'l',
            {{-8, -5}}
        }, {
            'c',
            {{8, -5}, {-5, -10}, {5,  -10}}
        }, {
            'l',
            {{1, 9}}
        }, {
            'l',
            {{-1, 9}}
        }
    }
};

static GPath *s_minute_sm;

static const GPathInfo MINUTE_SMALL = {
    5,
    (GPoint []) {
        {-1, 5},
        {-4, -4},
        {4, -4},
        {1, 5},
        {-1, 5}
    }
};

static GPath *s_oval;

static const GPathBuilderCommandList s_oval_comm = {
    5,
    {
        {
            'm',
            {{1, 9}}
        }, {
            'c',
            {{1, -9}, {8, 8}, {8, -8}}
        }, {
            'l',
            {{-1, -9}}
        }, {
            'c',
            {{-1, 9}, {-8, -8}, {-8, 8}}
        }, {
            'l',
            {{1, 9}}
        }
    }
};

static GPath *s_oval_sm;

static const GPathBuilderCommandList s_oval_sm_comm = {
    5,
    {
        {
            'm',
            {{1, 6}}
        }, {
            'c',
            {{1, -6}, {5, 5}, {5, -5}}
        }, {
            'l',
            {{-1, -6}}
        }, {
            'c',
            {{-1, 6}, {-5, -5}, {-5, 5}}
        }, {
            'l',
            {{1, 6}}
        }
    }
};

static GPath *s_omega;

static const GPathBuilderCommandList s_omega_comm = {
        6,
        {
            {
                'm',
                {{-6, 9}}
            }, {
                'c',
                {{-4, 7}, {-5, 9}, {-5, 8}}
            }, {
                'c',
                {{-1, -9}, {-8, 1}, {-8, -7}}
            }, {
                'l',
                {{1, -9}}
            }, {
                'c',
                {{4, 7}, {8, -7}, {8, 1}}
            }, {
                'c',
                {{6, 9}, {5, 8}, {5, 9}}
            }
        }
};

void draw_deco(GContext *ctx, GPath *deco, GPoint origin, float angle);
