#include <pebble.h>
#include "lib/pebble-assist.h"
#include "shapes.h"
#include "lib/math-sll.h"

static __inline__ double dbl(double x) {
    sll sval = dbl2sll(x);
    return *(double*)&sval;
}

#define NotBullshitTupletCString(_key, _cstring) \
    ((const Tuplet) { .type = TUPLE_CSTRING, .key = _key, .cstring = { .data = _cstring, .length = strlen(_cstring) + 1 }})

#define CLOCK_RADIUS 84
#define CLOCK_META_RADIUS 60

//the config keys
static AppSync s_sync;
static uint8_t s_sync_buffer[56];

#define CLOCK_FACE 0
const char CLOCK_FACE_DEFAULT[] = "ct";
static char s_clock_face_str[5];

#define BACKGROUND 1
const char BACKGROUND_DEFAULT[] = "stone";
static char s_background_str[10];

#define ROTATION 2
const char ROTATION_DEFAULT[] = "cw";
static char s_rotation_str[7];

// #define PALLET 3
// const char PALLET_DEFAULT[] = "1";
// static char s_pallet_str[3];

#define META 3
const bool META_DEFAULT = true;
static bool s_meta_bool;


typedef struct {
    int hours;
    int minutes;
    int hours_24;
} Time;

static Window *s_main_window;

static Layer *s_window_layer;
static Layer *s_minute_ring_layer;
static Layer *s_hour_face_layer;
static Layer *s_static_objects_layer;
static Layer *s_bluetooth_layer;
static Layer *s_watch_meta_layer;

static bool s_show_watch_meta = false;
static bool s_bluetooth_is_connected;
static BatteryChargeState s_battery_state;
static char s_battery_percent_str[4];

static Time s_last_time;

static GPoint s_center;

static GRect s_window_bounds;
static GRect s_dialog;
static GRect s_diamond_box;

static GFont s_dialog_font;
static GFont s_diamond_font;

static GBitmap *s_background;

static void tick_handler(struct tm *tick_time, TimeUnits units_changed) {
    // Store time
    s_last_time.hours = tick_time->tm_hour;
    s_last_time.hours -= (s_last_time.hours > 12) ? 12 : 0;
    s_last_time.hours_24 = tick_time->tm_hour;
    s_last_time.minutes = tick_time->tm_min;

    //have layers redraw
    if (s_minute_ring_layer) {
        layer_mark_dirty(s_minute_ring_layer);
    }
}

static void bluetooth_handler(bool connected) {
    if (!connected) {
        layer_show(s_bluetooth_layer);
        light_enable_interaction();
        vibes_short_pulse();
        vibes_long_pulse();
    } else {
        layer_hide(s_bluetooth_layer);
        vibes_double_pulse();
    }
}

static void toggle_meta(void *data) {
    s_show_watch_meta = !s_show_watch_meta;

    if (s_show_watch_meta) {
        layer_show(s_watch_meta_layer);
        layer_hide(s_minute_ring_layer);
        layer_hide(s_hour_face_layer);
        layer_hide(s_static_objects_layer);
    } else {
        layer_hide(s_watch_meta_layer);
        layer_show(s_minute_ring_layer);
        layer_show(s_hour_face_layer);
        layer_show(s_static_objects_layer);
    }
}

static void accel_handler(AccelAxisType axis, int32_t direction) {
    s_battery_state = battery_state_service_peek();
    s_bluetooth_is_connected = connection_service_peek_pebble_app_connection();
    snprintf(s_battery_percent_str, 4, "%d", s_battery_state.charge_percent);

    if (!s_show_watch_meta && s_meta_bool) {
        toggle_meta(NULL);
        app_timer_register(3000, toggle_meta, NULL);
    }
}

static GPoint get_point_on_circumference(double angle, int distance, int delta) {
    return (GPoint) {
      .x = (int16_t)(sin_lookup(angle) * (int32_t)(distance+delta) / TRIG_MAX_RATIO) + s_center.x,
      .y = (int16_t)(-cos_lookup(angle) * (int32_t)(distance+delta) / TRIG_MAX_RATIO) + s_center.y,
    };
}

static void window_update_proc(Layer *this_layer, GContext *ctx) {
    bool isBW = strcmp(s_clock_face_str, "dcbw") == 0 || strcmp(s_clock_face_str, "ctbw") == 0;
    //draw BACKGROUND
    if (strcmp(s_background_str, "stone") == 0) {
        graphics_draw_bitmap_in_rect(ctx, s_background, s_window_bounds);
    #ifdef PBL_COLOR
    } else if (strcmp(s_background_str, "stonebw") == 0) {
        graphics_draw_bitmap_in_rect(ctx, s_background, s_window_bounds);
    #endif
    } else if (strcmp(s_background_str, "black") == 0) {
        graphics_context_set_fill_color(ctx, GColorBlack);
        graphics_fill_rect(ctx, s_window_bounds, 0, GCornersAll);
    } else {
        graphics_context_set_fill_color(ctx, GColorWhite);
        graphics_fill_rect(ctx, s_window_bounds, 0, GCornersAll);
    }

    //draw minute ring
    if (isBW) {
        graphics_context_set_fill_color(ctx, GColorBlack);
    } else {
        graphics_context_set_fill_color(ctx, COLOR_FALLBACK(GColorWindsorTan, GColorBlack));
    }
    graphics_fill_circle(ctx, s_center, CLOCK_RADIUS);

    //draw clock face background
    if (strcmp(s_clock_face_str, "dc") == 0) {
        graphics_context_set_fill_color(ctx, COLOR_FALLBACK(GColorJaegerGreen, GColorWhite));
    } else if (isBW) {
        graphics_context_set_fill_color(ctx, GColorWhite);
    } else {
        graphics_context_set_fill_color(ctx, COLOR_FALLBACK(GColorCadetBlue, GColorWhite));
    }
    graphics_fill_circle(ctx, s_center, CLOCK_RADIUS-20);
};

static void minute_ring_update_proc(Layer *this_layer, GContext *ctx) {
    int minutes;
    bool isBW = strcmp(s_clock_face_str, "dcbw") == 0 || strcmp(s_clock_face_str, "ctbw") == 0;
    if (strcmp(s_rotation_str, "ccw") == 0 || strcmp(s_rotation_str, "cw-ccw") == 0) {
        minutes = 60 - s_last_time.minutes;
    } else {
        minutes = s_last_time.minutes;
    }
    double minute_angle = dbl(TRIG_MAX_ANGLE * minutes / 60);

    //draw the point where the minute marker would be
    GPoint minute_hand = get_point_on_circumference(minute_angle, CLOCK_RADIUS, -9);
    if (isBW) {
        graphics_context_set_fill_color(ctx, GColorWhite);
    } else {
        graphics_context_set_fill_color(ctx, COLOR_FALLBACK(GColorDarkCandyAppleRed, GColorWhite));
    }
    draw_deco(ctx, s_omega, minute_hand, minute_angle);
    #ifdef PBL_COLOR
        if (isBW) {
            graphics_context_set_fill_color(ctx, GColorWhite);
        } else {
            graphics_context_set_fill_color(ctx, GColorWindsorTan);
        }
        draw_deco(ctx, s_oval_sm, minute_hand, minute_angle);
    #endif

    //draw lines
    if (isBW) {
        graphics_context_set_stroke_color(ctx, GColorWhite);
    } else {
        graphics_context_set_stroke_color(ctx, COLOR_FALLBACK(GColorRajah, GColorWhite));
    }

    GPoint lines[2];
    GPoint dot;
    for (uint16_t i=0; i < 59; i++) {
        double curr_angle = dbl(TRIG_MAX_ANGLE * (minutes + i+1) / 60);

        if (i == 14 || i == 29 || i == 44) {
            //draw the three blue quarter markers
            lines[0] = get_point_on_circumference(curr_angle, CLOCK_RADIUS, -9);
            if (isBW) {
                graphics_context_set_fill_color(ctx, GColorWhite);
            } else {
                graphics_context_set_fill_color(ctx, COLOR_FALLBACK(GColorCobaltBlue, GColorWhite));
            }
            draw_deco(ctx, s_oval, lines[0], curr_angle);
            if (isBW) {
                graphics_context_set_fill_color(ctx, GColorBlack);
            } else {
                graphics_context_set_fill_color(ctx, COLOR_FALLBACK(GColorWindsorTan, GColorBlack));
            }
            draw_deco(ctx, s_oval_sm, lines[0], curr_angle);

        } else {
            int mod = (i+1) % 5;
            if(mod== 0) {
                //draw the five minute lines
                lines[0] = get_point_on_circumference(curr_angle, CLOCK_RADIUS, -9);
                if (isBW) {
                    graphics_context_set_fill_color(ctx, GColorWhite);
                } else {
                    graphics_context_set_fill_color(ctx, COLOR_FALLBACK(GColorRajah, GColorWhite));
                }
                draw_deco(ctx, s_minute, lines[0], curr_angle);
                if (isBW) {
                    graphics_context_set_fill_color(ctx, GColorBlack);
                } else {
                    graphics_context_set_fill_color(ctx, COLOR_FALLBACK(GColorWindsorTan, GColorBlack));
                }
                draw_deco(ctx, s_minute_sm, lines[0], curr_angle);
            } else  {
                //draw the lines
                if (mod == 1 || mod == 2 || mod == 3) {
                    //draw the dots
                    double dot_angle = dbl(TRIG_MAX_ANGLE * (minutes + i+dbl(1.5)) / 60);
                    dot = get_point_on_circumference(dot_angle, CLOCK_RADIUS, -5);
                    graphics_draw_pixel(ctx, dot);
                }
                lines[0] = get_point_on_circumference(curr_angle, CLOCK_RADIUS, -15);
                lines[1] = get_point_on_circumference(curr_angle, CLOCK_RADIUS, -7);
                graphics_draw_line(ctx, lines[0], lines[1]);
            }
        }
    }

}

static void hour_face_update_proc(Layer *this_layer, GContext *ctx) {
    int hours;
    bool isBW = strcmp(s_clock_face_str, "dcbw") == 0 || strcmp(s_clock_face_str, "ctbw") == 0;
    if (strcmp(s_rotation_str, "ccw") == 0 || strcmp(s_rotation_str, "ccw-cw") == 0) {
        hours = 12 - s_last_time.hours;
    } else {
        hours = s_last_time.hours;
    }

    int face_index = hours == 12 ? 0 : hours;

    #ifdef PBL_COLOR
        uint32_t id_array[12];
    #elif PBL_BW
        uint32_t id_array[12];
    #endif

    if (strcmp(s_clock_face_str, "dc") == 0) {
        memcpy(id_array, s_dc_ids, sizeof(id_array));
    #ifdef PBL_COLOR
    } else if (strcmp(s_clock_face_str, "dcbw") == 0) {
        memcpy(id_array, s_dcbw_ids, sizeof(id_array));
    } else if (strcmp(s_clock_face_str, "ctbw") == 0) {
        memcpy(id_array, s_ctbw_ids, sizeof(id_array));
    #endif
    } else {
        memcpy(id_array, s_ct_ids, sizeof(id_array));
    }

    //clean up old image
    gbitmap_destroy_safe(s_hours_face);
    s_hours_face = gbitmap_create_with_resource(id_array[face_index]);
    GRect face = gbitmap_get_bounds(s_hours_face);
    grect_align(&face, &s_window_bounds, GAlignCenter, false);

    graphics_context_set_compositing_mode(ctx, GCompOpSet);
    graphics_draw_bitmap_in_rect(ctx, s_hours_face, face);

    //draw other marker circles
    GPoint circle;
    for (uint16_t i = 0; i < 12; i++) {
        if (i == 3 || i == 9 || i == 0) {
            continue;
        }
        double curr_angle = dbl(TRIG_MAX_ANGLE * (hours + i) / 12);
        if (i == 6) {
            //draw day time indicator
            circle = get_point_on_circumference(curr_angle, CLOCK_RADIUS, -45);
            if (isBW) {
                graphics_context_set_fill_color(ctx, GColorBlack);
            } else {
                graphics_context_set_fill_color(ctx, COLOR_FALLBACK(GColorWindsorTan, GColorBlack));
            }
            graphics_fill_circle(ctx, circle, 17);

            gbitmap_destroy_safe(s_sun_moon);

            if (s_last_time.hours_24 >= 18 || s_last_time.hours_24 < 6) {
                //it's night
                #ifdef PBL_COLOR
                    if (isBW) {
                        s_sun_moon = gbitmap_create_with_resource(RESOURCE_ID_MOON_BW);
                    } else {
                        s_sun_moon = gbitmap_create_with_resource(RESOURCE_ID_MOON);
                    }
                #elif PBL_BW
                    s_sun_moon = gbitmap_create_with_resource(RESOURCE_ID_MOON_BW);
                #endif
            } else {
                //it's day time
                #ifdef PBL_COLOR
                    if (isBW) {
                        s_sun_moon = gbitmap_create_with_resource(RESOURCE_ID_SUN_BW);
                    } else {
                        s_sun_moon = gbitmap_create_with_resource(RESOURCE_ID_SUN);
                    }
                #elif PBL_BW
                    s_sun_moon = gbitmap_create_with_resource(RESOURCE_ID_SUN_BW);
                #endif
            }

            GRect sun_moon_rect = {
                .origin = GPoint(circle.x-15, circle.y-15),
                .size = {30, 30}
            };

            graphics_context_set_compositing_mode(ctx, GCompOpSet);
            graphics_draw_bitmap_in_rect(ctx, s_sun_moon, sun_moon_rect);

        } else {
            //draw the white circle markers
            if (isBW) {
                    graphics_context_set_fill_color(ctx, GColorBlack);
            } else {
                graphics_context_set_fill_color(ctx, COLOR_FALLBACK(GColorWhite, GColorBlack));
            }
            circle = get_point_on_circumference(curr_angle, CLOCK_RADIUS, -32);
            graphics_fill_circle(ctx, circle, 3);
        }
    }
}

static void static_objects_proc(Layer *this_layer, GContext *ctx) {
    bool isBW = strcmp(s_clock_face_str, "dcbw") == 0 || strcmp(s_clock_face_str, "ctbw") == 0;
    if (isBW) {
        graphics_context_set_fill_color(ctx, GColorBlack);
        graphics_context_set_stroke_color(ctx, GColorWhite);
    } else {
        graphics_context_set_fill_color(ctx, COLOR_FALLBACK(GColorWindsorTan, GColorBlack));
        graphics_context_set_stroke_color(ctx, COLOR_FALLBACK(GColorBlueMoon, GColorWhite));
    }

    graphics_fill_circle(ctx, s_center, 10);
    graphics_draw_circle(ctx, s_center, 7);

    if (isBW) {
        graphics_context_set_stroke_color(ctx, GColorBlack);
        graphics_context_set_fill_color(ctx, GColorWhite);
    } else {
        graphics_context_set_stroke_color(ctx, COLOR_FALLBACK(GColorDarkGray, GColorBlack));
        graphics_context_set_fill_color(ctx, COLOR_FALLBACK(GColorLightGray, GColorWhite));
    }
    gpath_draw_filled(ctx, s_arrow);
    gpath_draw_outline(ctx, s_arrow);
}

static void bluetooth_proc(Layer *this_layer, GContext *ctx) {
    s_dialog = (GRect) {
        .origin = {0, 0},
        .size = {134, 50}
    };
    graphics_context_set_fill_color(ctx, COLOR_FALLBACK(GColorPictonBlue, GColorBlack));
    grect_align(&s_dialog, &s_window_bounds, GAlignCenter, false);
    graphics_fill_rect(ctx, s_dialog, 0, GCornersAll);
    graphics_context_set_text_color(ctx, GColorWhite);
    #ifdef PBL_BW
        graphics_context_set_stroke_color(ctx, GColorWhite);
        graphics_draw_rect(ctx, s_dialog);
    #endif
    graphics_draw_text(ctx, "Bluetooth disconnected!", s_dialog_font, s_dialog, GTextOverflowModeWordWrap, GTextAlignmentCenter, NULL);
}

static void watch_meta_proc(Layer *this_layer, GContext *ctx) {
    // blank out everything
    graphics_context_set_fill_color(ctx, GColorBlack);
    graphics_fill_circle(ctx, s_center, CLOCK_RADIUS);

    // draw circle
    graphics_context_set_stroke_color(ctx, GColorWhite);
    graphics_context_set_fill_color(ctx, GColorWhite);
    graphics_draw_circle(ctx, s_center, CLOCK_META_RADIUS);

    for (uint16_t i = 0; i < 10; i++) {
        double curr_angle = dbl(TRIG_MAX_ANGLE * i / 10);
        graphics_draw_line(ctx,
            get_point_on_circumference(curr_angle, CLOCK_META_RADIUS, -60),
            get_point_on_circumference(curr_angle, CLOCK_META_RADIUS, 0)
        );
    }

    // draw deco
    GPoint meta_one_point = get_point_on_circumference(DEG_TO_TRIGANGLE(0), CLOCK_META_RADIUS, 12);
    GRect meta_one_rect = (GRect) {
        .origin = {meta_one_point.x-15, meta_one_point.y},
        .size = {31, 16}
    };

    GPoint meta_two_point = get_point_on_circumference(DEG_TO_TRIGANGLE(180), CLOCK_META_RADIUS, 0);
    GRect meta_two_rect = (GRect) {
        .origin = {meta_two_point.x-15, meta_two_point.y},
        .size = {31, 16}
    };

    gbitmap_destroy_safe(s_meta_one);
    gbitmap_destroy_safe(s_meta_two);

    s_meta_one = gbitmap_create_with_resource(RESOURCE_ID_META_ONE);
    s_meta_two = gbitmap_create_with_resource(RESOURCE_ID_META_TWO);

    graphics_context_set_compositing_mode(ctx, GCompOpSet);
    graphics_draw_bitmap_in_rect(ctx, s_meta_one, meta_one_rect);
    graphics_draw_bitmap_in_rect(ctx, s_meta_two, meta_two_rect);

    // draw percent diamond
    graphics_context_set_fill_color(ctx, COLOR_FALLBACK(GColorDarkGreen, GColorWhite));
    gpath_move_to(s_diamond, s_center);
    gpath_draw_filled(ctx, s_diamond);
    #ifdef PBL_COLOR
        graphics_context_set_stroke_color(ctx, GColorJaegerGreen);
        gpath_draw_outline(ctx, s_diamond);
    #endif
    graphics_context_set_text_color(ctx, COLOR_FALLBACK(GColorWhite, GColorBlack));

    s_diamond_box = (GRect) {
        .origin = {0, 0},
        .size = {40, 24}
    };
    grect_align(&s_diamond_box, &s_window_bounds, GAlignCenter, false);
    graphics_draw_text(ctx, s_battery_percent_str, s_diamond_font, s_diamond_box, GTextOverflowModeWordWrap, GTextAlignmentCenter, NULL);

    // draw bluetooth indicator
    double bluetooth_angle = dbl(TRIG_MAX_ANGLE * (s_battery_state.charge_percent - 50) / 100);
    GPoint bluetooth_point = get_point_on_circumference(bluetooth_angle, CLOCK_META_RADIUS, 0);
    GRect bluetooth_box = (GRect) {
        .origin = {bluetooth_point.x-15, bluetooth_point.y-15},
        .size = {30, 30}
    };

    graphics_context_set_fill_color(ctx, COLOR_FALLBACK(GColorBlue, GColorWhite));
    graphics_fill_circle(ctx, bluetooth_point, 15);

    //clean up old image
    gbitmap_destroy_safe(s_bluetooth_bitmap);
    #ifdef PBL_COLOR
        if (s_bluetooth_is_connected) {
            s_bluetooth_bitmap = gbitmap_create_with_resource(RESOURCE_ID_BLUETOOTH);
        } else {
            s_bluetooth_bitmap = gbitmap_create_with_resource(RESOURCE_ID_BLUETOOTH_DISCONNECTED);
        }
    #elif PBL_BW
        if (s_bluetooth_is_connected) {
            s_bluetooth_bitmap = gbitmap_create_with_resource(RESOURCE_ID_BLUETOOTH_BW);
        } else {
            s_bluetooth_bitmap = gbitmap_create_with_resource(RESOURCE_ID_BLUETOOTH_DISCONNECTED_BW);
        }
    #endif
    graphics_context_set_compositing_mode(ctx, GCompOpSet);
    graphics_draw_bitmap_in_rect(ctx, s_bluetooth_bitmap, bluetooth_box);
}

static void setBackgroundBitmap() {
    if (strcmp(s_background_str, "stone") == 0) {
        gbitmap_destroy_safe(s_background);
        #ifdef PBL_BW
        s_background = gbitmap_create_with_resource(RESOURCE_ID_STONE_TILE_BW);
        #elif PBL_COLOR
        s_background = gbitmap_create_with_resource(RESOURCE_ID_STONE_TILE);
        #endif
    #ifdef PBL_COLOR
    } else if (strcmp(s_background_str, "stonebw") == 0) {
        gbitmap_destroy_safe(s_background);
        s_background = gbitmap_create_with_resource(RESOURCE_ID_STONE_TILE_BW);
    #endif
    }
}

static void main_window_load(Window *window) {
    //get bounds and window layer
    s_window_layer = window_get_root_layer(window);
    s_window_bounds = layer_get_bounds(s_window_layer);

    s_center = grect_center_point(&s_window_bounds);

    // load fonts
    s_dialog_font = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_RETURN_OF_GANON_22));
    s_diamond_font = fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD);

    //create layers
    s_minute_ring_layer = layer_create(s_window_bounds);
    s_hour_face_layer = layer_create(s_window_bounds);
    s_static_objects_layer = layer_create(s_window_bounds);
    s_bluetooth_layer = layer_create(s_window_bounds);
    s_watch_meta_layer = layer_create(s_window_bounds);

    //create deco
    s_oval = gpath_builder_create_path_from_commands(&s_oval_comm);
    s_oval_sm = gpath_builder_create_path_from_commands(&s_oval_sm_comm);
    s_omega = gpath_builder_create_path_from_commands(&s_omega_comm);
    s_arrow = gpath_create(&ARROW);
    s_minute = gpath_builder_create_path_from_commands(&s_minute_comm);
    s_minute_sm = gpath_create(&MINUTE_SMALL);
    s_diamond = gpath_create(&DIAMOND);

    setBackgroundBitmap();

    //set their update listeners
    layer_set_update_proc(s_window_layer, window_update_proc);
    layer_set_update_proc(s_minute_ring_layer, minute_ring_update_proc);
    layer_set_update_proc(s_hour_face_layer, hour_face_update_proc);
    layer_set_update_proc(s_static_objects_layer, static_objects_proc);
    layer_set_update_proc(s_bluetooth_layer, bluetooth_proc);
    layer_set_update_proc(s_watch_meta_layer, watch_meta_proc);

    //add layers to window
    layer_add_child(s_window_layer, s_hour_face_layer);
    layer_add_child(s_window_layer, s_minute_ring_layer);
    layer_add_child(s_window_layer, s_static_objects_layer);
    layer_add_child(s_window_layer, s_bluetooth_layer);
    layer_add_child(s_window_layer, s_watch_meta_layer);

    layer_hide(s_bluetooth_layer);
    layer_hide(s_watch_meta_layer);
}

static void main_window_unload(Window *window) {
    //images
    gbitmap_destroy(s_bluetooth_bitmap);
    gbitmap_destroy(s_hours_face);
    gbitmap_destroy(s_sun_moon);
    gbitmap_destroy(s_meta_one);
    gbitmap_destroy(s_meta_two);
    gbitmap_destroy(s_background);

    //destroy deco
    gpath_destroy(s_oval);
    gpath_destroy(s_oval_sm);
    gpath_destroy(s_omega);
    gpath_destroy(s_arrow);
    gpath_destroy(s_minute);
    gpath_destroy(s_minute_sm);
    gpath_destroy(s_diamond);

    //kill layers
    layer_destroy_safe(s_minute_ring_layer);
    layer_destroy_safe(s_hour_face_layer);
    layer_destroy_safe(s_static_objects_layer);
    layer_destroy_safe(s_bluetooth_layer);
}

static void sync_changed_handler(const uint32_t key, const Tuple *new_tuple, const Tuple *old_tuple, void *context) {
    switch (key) {
        case CLOCK_FACE:
            // DEBUG("Pebble got: %u CLOCK_FACE: %s", (uint)CLOCK_FACE, new_tuple->value->cstring);
            persist_write_string(CLOCK_FACE, (char *)new_tuple->value->cstring);
            strcpy(s_clock_face_str, new_tuple->value->cstring);
            break;
        case BACKGROUND:
            // DEBUG("Pebble got: %u BACKGROUND: %s", (uint)BACKGROUND, new_tuple->value->cstring);
            persist_write_string(BACKGROUND, (char *)new_tuple->value->cstring);
            strcpy(s_background_str, new_tuple->value->cstring);
            setBackgroundBitmap();
            break;
        case ROTATION:
            // DEBUG("Pebble got: %u ROTATION: %s", (uint)ROTATION, new_tuple->value->cstring);
            persist_write_string(ROTATION, (char *)new_tuple->value->cstring);
            strcpy(s_rotation_str, new_tuple->value->cstring);
            break;
        case META:
            // DEBUG("Pebble got: %u META: %d", (uint)META, (bool)new_tuple->value->uint16);
            persist_write_bool(META, (bool)new_tuple->value->uint16);
            s_meta_bool = (bool)new_tuple->value->uint16;
            break;
    }

    //have layers redraw
    if (s_minute_ring_layer) {
        layer_mark_dirty(s_minute_ring_layer);
    }
};

static void sync_error_handler(DictionaryResult dict_error, AppMessageResult app_message_error, void *context) {
    ERROR("There was a sync Error!");
};

static void init() {
    //config stuff
    if (persist_exists(CLOCK_FACE)) {
        persist_read_string(CLOCK_FACE, s_clock_face_str, sizeof(s_clock_face_str));
    } else {
        strcpy(s_clock_face_str, CLOCK_FACE_DEFAULT);
    }

    if (persist_exists(BACKGROUND)) {
        persist_read_string(BACKGROUND, s_background_str, sizeof(s_background_str));
    } else {
        strcpy(s_background_str, BACKGROUND_DEFAULT);
    }

    if (persist_exists(ROTATION)) {
        persist_read_string(ROTATION, s_rotation_str, sizeof(s_rotation_str));
    } else {
        strcpy(s_rotation_str, ROTATION_DEFAULT);
    }

    // if (persist_exists(PALLET)) {
    //     persist_read_string(PALLET, s_pallet_str, sizeof(s_pallet_str));
    // } else {
    //     strcpy(s_pallet_str, PALLET_DEFAULT);
    // }

    if (persist_exists(META)) {
        s_meta_bool = persist_read_bool(META);
    } else {
        s_meta_bool = META_DEFAULT;
    }

    Tuplet config[] = {
        NotBullshitTupletCString(CLOCK_FACE, s_clock_face_str),
        NotBullshitTupletCString(BACKGROUND, s_background_str),
        NotBullshitTupletCString(ROTATION, s_rotation_str),
        TupletInteger(META, s_meta_bool)
    };

    int size = dict_calc_buffer_size_from_tuplets(config, 4);
    app_message_open(size, size);

    app_sync_init(&s_sync, s_sync_buffer, sizeof(s_sync_buffer), config, ARRAY_LENGTH(config), sync_changed_handler, sync_error_handler, NULL);

    //immediately update time
    time_t temp = time(NULL);
    struct tm *tick_time = localtime(&temp);
    tick_handler(tick_time, MINUTE_UNIT);

    //this is the start window stuff
    s_main_window = window_create();
    window_set_window_handlers(s_main_window, (WindowHandlers) {
        .load = main_window_load,
        .unload = main_window_unload
    });
    window_stack_push(s_main_window, true);

    //various listeners
    tick_timer_service_subscribe(MINUTE_UNIT, tick_handler);
    bluetooth_connection_service_subscribe(bluetooth_handler);
    accel_tap_service_subscribe(accel_handler);
}

static void deinit() {
    window_destroy_safe(s_main_window);

    app_sync_deinit(&s_sync);
}

int main(void) {
    init();
    app_event_loop();
    deinit();
}
