#include <pebble.h>
#include "lib/pebble-assist.h"
#include "shapes_util.h"

GPath *gpath_builder_create_path_from_commands(const GPathBuilderCommandList *command_list) {
    GPathBuilder *builder = gpath_builder_create(MAX_POINTS);
    for(uint32_t i = 0; i < command_list->num_commands; i++) {
        switch(command_list->commands[i].command) {
            case 'm':
                gpath_builder_move_to_point(builder, command_list->commands[i].points[0]);
                break;
            case 'c':
                gpath_builder_curve_to_point(builder, command_list->commands[i].points[0], command_list->commands[i].points[1], command_list->commands[i].points[2]);
                break;
            case 'l':
                gpath_builder_line_to_point(builder, command_list->commands[i].points[0]);
                break;
        }
    }
    GPath *path = gpath_builder_create_path(builder);
    gpath_builder_destroy(builder);
    return path;
};
